require 'bundler/setup'
require 'scythe'

class EmployeesSchema < Scythe::Schema
  field :id
  field :last_name
  field :middle_name
  field :first_name
  field :street
  field :city
  field :state
  field :zip
  field :phone
  field :email
  field :title
  field :salary

  virtual :full_name do |e|
    "#{e.last_name}, #{e.first_name} #{e.middle_name[0]}."
  end
end

EmployeesSchema.new('employees.csv').each do |e|
  puts e.full_name
end
