require 'bundler/setup'
require 'scythe'

class EmployeesSchema < Scythe::Schema
  field :id
  field :last_name
  field :middle_name
  field :first_name
  field :street
  field :city
  field :state
  field :zip
  field :phone do |x|
    # change from `xxx-yyy-zzzz` to `(xxx) yyy-zzzz`
    "(#{x[0..2]}) #{x[4..6]}-#{x[8..-1]}"
  end
  field :email
  field :title
  field :salary
end

EmployeesSchema.new('employees.csv').each do |e|
  puts e.phone
end
