module Scythe
  class Record
    def initialize(fields, csv_row)
      @fields = fields
      @csv_row = csv_row
    end

    def method_missing(name, *args, &block)
      field = @fields.select { |x| x.name == name.to_s }.first

      super unless field

      field_index = @fields.map { |x| x.input_name }.compact.index field.input_name

      field.render(field_index.nil? ? self : @csv_row[field_index])
    end
  end
end
