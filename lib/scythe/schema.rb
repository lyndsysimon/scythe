require 'CSV'

module Scythe
  class Schema
    include Enumerable

    def initialize(file)
      @file = file
    end

    class << self
      def fields
        raise NoMethodError "Undefined method `fields' for #{self}" if self == Scythe::Schema
        @fields ||= []
      end

      def field(*args, &block)
        fields << Scythe::Field.new(*args, &block)
      end

      def virtual(name, &block)
        fields << Scythe::Field.new(name, from: nil, &block)
      end
    end

    def fields
      self.class.fields
    end

    def each
      CSV.foreach @file, headers: includes_headers? do |csv_row|
        yield Scythe::Record.new(fields, csv_row)
      end
    end

    private

    def includes_headers?
      # TODO: What if the headers match the first data row?
      CSV.parse open(@file).readline do |first_row|
        return first_row == self.class.fields.map { |x| x.input_name }
      end
    end
  end
end
