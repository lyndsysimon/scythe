module Scythe
  class Field
    attr_reader :type

    def initialize(name, options = {}, &block)
      @name = name
      @options = options
      @block = block
    end

    def input_name
      if @options.include?(:from) && @options[:from].nil?
        nil
      elsif @options[:from].nil?
        @name.to_s
      else
        @options[:from].to_s
      end
    end

    def name
      @name.to_s
    end

    def render(value)
      # Call the block passed to the field definition
      value = @block.call(value) unless @block.nil?

      # coerce type
      value = method(@options[:type].name.to_sym).call(value) if @options.include? :type

      value
    end
  end
end
