# Scythe

Scythe is intended to make working with CSV files easier.

## Usage

The simplest way to use Scythe is to open a CSV directly and iterate the records:

```ruby
Scythe.open('employees.csv').each do |row|
  employee = Employee.find_or_create_by id: row.id
  employee.update first_name: row.first_name,
                  last_name: row.last_name
end
```

For a slightly more useful case, let's parse the following CSV:

```csv
Employee ID,First Name,Last Name,Position
1,Bob,Belcher,owner
2,Tina,Belcher,cook
3,Gene,Belcher,mascot
```

```ruby
class EmployeeSchema < Scythe::Schema
  field :id, from: 'Employee ID'
  field :given_name, from: 'First Name'
  field :family_name, from: 'Last Name'
  field :job, from: 'Position'
end

employees = EmployeeSchema.new('employees.csv')
```

Now we can print a summary:

```ruby
employees.each do |e|
  puts "Employee ##{e.id} is #{e.given_name} #{e.family_name}. Their job is '#{e.job}'."
end
# > Employee #1 is Bob Belcher. Their job is 'owner'.
# > Employee #2 is Tina Belcher. Their job is 'cook'.
# > Employee #3 is Gene Belcher. Their job is 'mascot'.
```

We can filter like any other Enumerable:

```ruby
employees.select { |e| e.job == 'cook' }.each { |e| puts e.given_name }
# > Tina

employees.reject { |e| e.job == 'owner'}.map { |e| puts e.given_name }
# > ['Tina', 'Gene']
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
