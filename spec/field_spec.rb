RSpec.describe Scythe::Field do
  describe 'with no options' do
    subject { Scythe::Field.new :foo }

    it 'instantiates without options' do
      expect { subject }.to_not raise_error
    end

    it 'exposes name' do
      expect(subject.name).to eq 'foo'
    end

    it 'return the name for the #input_name' do
      expect(subject.input_name).to eq 'foo'
    end

    it 'renders properly' do
      expect(subject.render 'x').to eq 'x'
    end
  end

  describe 'with :from passed' do
    subject { Scythe::Field.new :foo, from: :bar }

    it 'returns the value of :from for #input_name' do
      expect(subject.input_name).to eq 'bar'
    end
  end

  describe 'with :from explicitly passed as nil' do
    subject do
      Scythe::Field.new :foo, from: nil do |x|
        "-#{x}-"
      end
    end

    it 'renders properly' do
      expect(subject.render 'x').to eq '-x-'
    end
  end

  describe 'with a block passed' do
    subject do
      Scythe::Field.new :foo do |x|
        "-#{x}-"
      end
    end

    it 'renders properly' do
      expect(subject.render 'x').to eq '-x-'
    end
  end

  describe 'with a primitive type passed' do
    it 'renders properly for Integer' do
      expect(Scythe::Field.new(:foo, type: Integer).render('1')).to eq 1
    end

    it 'renders properly for String' do
      expect(Scythe::Field.new(:foo, type: String).render('1')).to eq '1'
    end
  end
end
