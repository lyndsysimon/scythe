require 'CSV'

RSpec.describe Scythe::Record do
  subject do
    Scythe::Record.new(
      [
        Scythe::Field.new(:id, type: Integer),
        Scythe::Field.new(:first_name),
        Scythe::Field.new(:family_name, from: :last_name),
      ],
      CSV::Row.new(['id', 'first_name', 'last_name'],
                   ['1', 'Lyndsy', 'Simon'])
    )
  end

  it 'allows attribute access' do
    expect(subject.first_name).to eq 'Lyndsy'
  end

  it 'performs type coercion' do
    expect(subject.id.class).to eq Integer
  end

  it 'raises an error for unknown attributes' do
    expect { subject.invalid }.to raise_error NoMethodError
  end

  it 'renames fields' do
    expect(subject.family_name).to eq 'Simon'
  end
end
