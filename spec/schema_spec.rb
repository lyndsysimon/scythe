RSpec.describe Scythe::Schema do
  let(:subject_class) { Scythe::Schema }
  it 'raises an exception for .fields' do
    expect { subject_class.fields }.to raise_error NoMethodError
  end

  it 'has no fields by default' do
    expect(Class.new(Scythe::Schema).fields).to eq []
  end

  context 'a subclass with explicit field definitions' do
    before :all do
      class TestSchema < Scythe::Schema
        field 'id'
        field 'first_name'
        field 'last_name'
      end
    end

    subject { TestSchema }

    it 'saves field definitions' do
      expect(subject.fields.count).to eq 3
    end

    describe '#include_headers?' do
      it 'is true if the headers match' do
        expect(
          subject.new('spec/static/simple.csv').send :includes_headers?
        ).to be true
      end

      it 'is false if there are no headers' do
        expect(
          subject.new('spec/static/no_headers.csv').send :includes_headers?
        ).to be false
      end

      it 'is false if the headers don\'t match' do
        expect(
          subject.new('spec/static/reordered_headers.csv').send :includes_headers?
        ).to be false
      end
    end

    it 'parses a simple CSV' do
      first, second = subject.new('spec/static/simple.csv').entries
      expect(first.class).to eq Scythe::Record
      expect(first.id).to eq '1'
      expect(first.first_name).to eq 'Lyndsy'
      expect(first.last_name).to eq 'Simon'

      expect(second.class).to eq Scythe::Record
      expect(second.id).to eq '2'
      expect(second.first_name).to eq 'John'
      expect(second.last_name).to eq 'Doe'
    end

    it 'parses a CSV without headers' do
      first, second = subject.new('spec/static/no_headers.csv').entries
      expect(first.class).to eq Scythe::Record
      expect(first.id).to eq '1'
      expect(first.first_name).to eq 'Lyndsy'
      expect(first.last_name).to eq 'Simon'

      expect(second.class).to eq Scythe::Record
      expect(second.id).to eq '2'
      expect(second.first_name).to eq 'John'
      expect(second.last_name).to eq 'Doe'
    end
  end
end
